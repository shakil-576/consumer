package com.consumer.demo.constants;

public class AppConstants {
    public static final String SMS = "SMS";
    public static final String WHATSAPP = "WHATSAPP";
    public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";
    public static final String TOPIC_NAME_EMAIL = "EMAIL";
    public static final String TOPIC_NAME_PHONE = "PHONE";
    public static final String CHANNEL_NAME_EMAIL = "Email";
    public static final String CHANNEL_NAME_PHONE = "Phone";
}
