package com.consumer.demo.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AppUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T getBeanByJson(String json, Class<T> valueType) {
        try {
            return objectMapper.readValue(json, valueType);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void printMessage(String subscriberIdentity, String channel, String leadId, String msg) {
        String formattedMsg = String.format("Subscriber %s: [Channel: %s] [LeadID: %s] [Message: %s]",
                subscriberIdentity, channel, leadId, msg);

        System.out.println(formattedMsg);
    }
}
