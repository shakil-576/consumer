package com.consumer.demo.service;

public interface MobileCommunicationMode {
    void sendToMobile(Integer leadId);
}
