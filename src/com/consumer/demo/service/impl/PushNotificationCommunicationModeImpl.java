package com.consumer.demo.service.impl;

import com.consumer.demo.constants.AppConstants;
import com.consumer.demo.service.MobileCommunicationMode;
import com.consumer.demo.util.AppUtil;

public class PushNotificationCommunicationModeImpl implements MobileCommunicationMode {
    @Override
    public void sendToMobile(Integer leadId) {
        AppUtil.printMessage(
                "2",
                AppConstants.CHANNEL_NAME_PHONE,
                String.valueOf(leadId),
                "Push Notification Sent"
        );
    }
}
