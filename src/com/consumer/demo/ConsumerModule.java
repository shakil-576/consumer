package com.consumer.demo;

import com.consumer.demo.constants.AppConstants;
import com.consumer.demo.dto.LeadPublisherReqDto;
import com.consumer.demo.factory.CommunicationFactory;
import com.consumer.demo.service.MobileCommunicationMode;
import com.consumer.demo.util.AppUtil;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerModule {
    public static final String KAFKA_SERVER_URL = "localhost";
    public static final int KAFKA_SERVER_PORT = 9092;
    public static final String CLIENT_ID = "ConsumerModule";
    private static CommunicationFactory factory = null;

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVER_URL + ":" + KAFKA_SERVER_PORT);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization" +
                ".IntegerDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization" +
                ".StringDeserializer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, CLIENT_ID);

        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(AppConstants.TOPIC_NAME_EMAIL, AppConstants.TOPIC_NAME_PHONE));

        factory = new CommunicationFactory();

        while (true) {
            pollConsumerRecords(consumer);
        }
    }

    private static void pollConsumerRecords(Consumer<String, String> consumer) {
        Duration pollDuration = Duration.ofMillis(100);
        ConsumerRecords<String, String> records = consumer.poll(pollDuration);

        for (ConsumerRecord<String, String> recordData : records) {
            LeadPublisherReqDto reqDto = AppUtil.getBeanByJson(recordData.value(), LeadPublisherReqDto.class);
            
            if (AppConstants.TOPIC_NAME_EMAIL.equals(recordData.topic())) {
                AppUtil.printMessage(
                        "1",
                        AppConstants.CHANNEL_NAME_EMAIL,
                        String.valueOf(reqDto.getLeadId()),
                        "Email Sent"
                );
            } else if (AppConstants.TOPIC_NAME_PHONE.equals(recordData.topic())) {
                String communicationMode = reqDto.getPreferredMobileCommunicationMode();

                MobileCommunicationMode communication = factory.getCommunication(communicationMode);

                communication.sendToMobile(reqDto.getLeadId());
            }
        }
    }
}
