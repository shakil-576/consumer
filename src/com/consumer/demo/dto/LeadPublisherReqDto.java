package com.consumer.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeadPublisherReqDto {

    @JsonProperty("lead_id")
    private Integer leadId;

    @JsonProperty("preferred_mobile_communication_mode")
    private String preferredMobileCommunicationMode;

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getPreferredMobileCommunicationMode() {
        return preferredMobileCommunicationMode;
    }

    public void setPreferredMobileCommunicationMode(String preferredMobileCommunicationMode) {
        this.preferredMobileCommunicationMode = preferredMobileCommunicationMode;
    }
}
