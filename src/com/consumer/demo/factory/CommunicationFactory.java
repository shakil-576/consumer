package com.consumer.demo.factory;

import com.consumer.demo.constants.AppConstants;
import com.consumer.demo.service.MobileCommunicationMode;
import com.consumer.demo.service.impl.PushNotificationCommunicationModeImpl;
import com.consumer.demo.service.impl.SMSCommunicationModeImpl;
import com.consumer.demo.service.impl.WhatsAppCommunicationModeImpl;

import java.util.HashMap;
import java.util.Map;

public class CommunicationFactory {
    private final Map<String, MobileCommunicationMode> communicationModes = new HashMap<>();

    public CommunicationFactory() {
        communicationModes.put(AppConstants.SMS, new SMSCommunicationModeImpl());
        communicationModes.put(AppConstants.WHATSAPP, new WhatsAppCommunicationModeImpl());
        communicationModes.put(AppConstants.PUSH_NOTIFICATION, new PushNotificationCommunicationModeImpl());
    }

    public MobileCommunicationMode getCommunication(String mode) {
        return communicationModes.get(mode);
    }
}
